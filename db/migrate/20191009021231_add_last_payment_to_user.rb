class AddLastPaymentToUser < ActiveRecord::Migration[6.0]
  change_table :users do |t|
    t.datetime :last_payment
  end
end
