class Admin::AdminController < ApplicationController

  before_action :authenticate_user!, :authenticate_admin!

  protected

  def authenticate_admin!
    unless current_user.type == 'User::Admin'
      flash[:alert] = "You are not authorized to access this page"
      redirect_to :root
    end
  end
end
