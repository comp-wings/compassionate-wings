class Admin::StreamCodesController < Admin::AdminController
    def index
        @latest = StreamCodes.last
        render layout: 'layouts/admin'
    end

    def update
        id = params[:id]
        StreamCodes.create(code: id)
    end

    private 

    def authenticate_admin!
        unless current_user.type == 'User::Admin'
            flash[:alert] = "You are not authorized to access this page"
            redirect_to :root
        end
    end

end
