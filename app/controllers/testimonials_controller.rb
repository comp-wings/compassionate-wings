class TestimonialsController < ApplicationController
  before_action :authenticate_user!, except: :index

  def index
    @testimonials = Testimonial.where(visible: true)
  end

  def create
    Testimonial.create(content: params[:content], visible: false, user_id: current_user.id)

    flash[:alert] = "Thank you for posting a testimonial! Look for it up on the Testimonials page sometime soon!"
    redirect_to :root
  end

  def new
  end

  def edit
  end

  def update
  end

  def destroy
  end

end
