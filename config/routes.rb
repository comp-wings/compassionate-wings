Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'welcome#index'

  get 'profile', to: 'profile#dashboard', as: :profile
  scope '/profile' do
    get 'upgrade', to: 'profile#upgrade_form'
    post 'upgrade', to: 'profile#upgrade'
  end

  get 'about', to: 'welcome#about', as: :about
  get 'services', to: 'welcome#services', as: :services
  get 'contact', to: 'welcome#contact', as: :contact
  get 'stream', to: 'welcome#stream', as: :stream
  get 'privacy', to: 'welcome#privacy', as: :privacy

  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

  resources :articles, only: [:index, :show]

  resources :testimonials, except: [:show]

  resources :appointments


  namespace :admin do
    resources :articles do
      post 'toggle', to: 'articles#toggle', param: :id
    end

    resources :testimonials, only: [:index] do
      post 'toggle', to: 'testimonials#toggle', param: :id
    end

    get 'stream', to: 'stream_codes#index', as: :stream
    post 'stream/update', to: 'stream_codes#update', as: :stream_update
  end

  
end
