class AddArticlePremiumHideColumn < ActiveRecord::Migration[6.0]
  change_table :articles do |t|
    t.boolean :hidden, default: false
    t.boolean :premium
  end
end
