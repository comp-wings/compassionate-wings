class ArticlesController < ApplicationController

    def index
        @articles = Article.where(hidden: false)
    end

    def show
        @article = Article.find(params[:id])

        # User clicked on a premium article
        if @article.premium
            # Direct them to user registration if they are not signed in
            unless user_signed_in?
                flash[:alert] = "Sign in to your premium account to view the full article"
                redirect_to :new_user_session
                return
            end

            # TODO: If they are logged in but not a member, redirect to User type control panel
            if current_user.type == 'User::Subscriber'
                flash[:alert] = "This is a premium members only article - please purchase a membership to read the whole article"
                redirect_to :root
            end
        end

    end
    
end
