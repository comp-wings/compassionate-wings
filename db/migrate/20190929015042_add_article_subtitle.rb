class AddArticleSubtitle < ActiveRecord::Migration[6.0]
  change_table :articles do |t|
    t.string :subtitle
  end
end
