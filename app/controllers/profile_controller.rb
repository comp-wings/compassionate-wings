class ProfileController < ApplicationController
    before_action :authenticate_user!
    skip_before_action :verify_authenticity_token

    def dashboard
    end

    def upgrade_form
        redirect_to :profile if current_user.type == 'User::Member' 
    end

    # Post
    def upgrade
        user = current_user

        customer = {
            name: user.name,
            email_address: user.email
        }

        card = {
            billing_address: {
                address_line_1: params[:address_line_1],
                address_line_2: params[:address_line_2],
                locality: params[:locality],
                administrative_district_level_1: params[:administrative_district_level_1],
                postal_code: params[:postal_code],
                country: params[:country],
            },
            cardholder_name: params[:cardholder_name],
            card_nonce: params[:nonce]
        }
        
        # { customer_info: json, card_info: json }
        customer_data = Membership::Premium.upgrade_user(customer, card)
        
        user.update(customer_data)

        render json: { success: true, message: 'Membership Enrollment Successful' }
    rescue => e
        render json: { success: false, message: e.message }
    end
end
