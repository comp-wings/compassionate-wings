class AddNameToUser < ActiveRecord::Migration[6.0]
  change_table :users do |t|
    t.string :name
  end
end
