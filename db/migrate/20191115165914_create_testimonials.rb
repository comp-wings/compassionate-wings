class CreateTestimonials < ActiveRecord::Migration[6.0]
  def change
    create_table :testimonials do |t|
      t.text :content
      t.string :name
      t.boolean :visible


      t.references :user
      t.timestamps
    end
  end
end
