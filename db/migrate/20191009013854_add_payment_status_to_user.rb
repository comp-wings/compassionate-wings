class AddPaymentStatusToUser < ActiveRecord::Migration[6.0]
  change_table :users do |t|
    t.string :payment_status
  end
end
