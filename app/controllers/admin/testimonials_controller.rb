class Admin::TestimonialsController <  Admin::AdminController
  def index
    @testimonials = Testimonial.all
    render layout: 'layouts/admin'
  end

  def toggle
    Testimonial.find(params[:testimonial_id]).toggle!(:visible)

    redirect_to :admin_testimonials
  end
end
