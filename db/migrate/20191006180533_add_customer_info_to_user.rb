class AddCustomerInfoToUser < ActiveRecord::Migration[6.0]
  change_table :users do |t|
    t.json :customer_info
    t.json :card_info
  end
end
