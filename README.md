
# README

  
Hey guys I'll try to make it as clear and concise as possible how to set up you environment.

As I will be taking care of the back-end its not crucial that you get everything up and running - but you can get a better idea of how things work and will look if everything is up and running.

1. [Install Ruby/Rails](https://gorails.com/setup/windows/10)
2. Clone git repo
```
git clone git@github.com:jason-meredith/compassionate-wings.git
```
3. Add your database credentials into `config/database.yml` I created a mysql user named 'comp-wing' with passowrd 'password-cw88' but you can make it whatever you want as long as it matches up in `database.yml`
4. Install gems: `bundle install`
5. Create database with `rake db:create`
6. Run the db migrations: `rake db:migrate`
7. Start the server: `rails server`
8. If everything worked you should be able to see it in your browser [here](http://localhost:3000)

To see all the currently implemented routes run `rake routes`