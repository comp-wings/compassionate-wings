class UserMembershipType < ActiveRecord::Migration[6.0]
  change_table :users do |t|
    t.integer :type
  end
end
