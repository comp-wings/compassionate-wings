class StreamCode < ActiveRecord::Migration[6.0]
  def change
    create_table :stream_codes do |t|
      t.string :code
      t.boolean :active

      t.timestamps
    end
  end
end
