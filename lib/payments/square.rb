require 'square'

module Payments
    module Square
        class << self
            def client
                ::Square::Client.new(
                    access_token: ENV['SQUARE_TOKEN'],
                    environment: ENV['SQUARE_ENVIRONMENT']
                  )
            end

            def location
                ENV['SQUARE_LOCATION']
            end


            # Pass hash with keys for:
            # * customer_id - pass User.customer_id
            # * cc - pass User.cc['id']
            # * charge_id - idempotentcy key, use SecureRandom.uuid
            # * amount - amount to charge in cents
            # * currency - defaults to CAD
            # * description - charge description
            #
            # @returns [Symbol] payment status
            def charge_user(values = {})
                customer_id = values[:customer_id]
                cc = values[:cc]

                charge = {
                    source_id: cc,
                    idempotency_key: values[:charge_id],
                    amount_money: {
                        amount: values[:amount],
                        currency:  values[:currency] || 'CAD',
                    },
                    customer_id: customer_id,
                    location_id: Payments::Square.location,
                    reference_id: SecureRandom.uuid,
                    note: values[:description]
                }

                payment = payments.create_payment(body: charge)

                payment.data.payment[:status].to_sym
            end

            private 

            def payments
                Payments::Square.client.payments
            end
        end
    end
end
