class WelcomeController < ApplicationController

    before_action :authenticate_member, only: :stream

    def index
        if signed_in?
            @user = current_user
        end

        @show_modal = true unless session[:cookies_confirm]

        session[:cookies_confirm] = true

    end
    
    def services
    end

    def about
    end

    def privacy

    end

    def contact
    end

    def stream
        @video_url = StreamCodes.last&.code
    end
    

    def authenticate_member
        return redirect_to new_user_session_path unless signed_in?

        unless ['User::Admin', 'User::Member'].include?(current_user.type)
            flash[:alert] = "You must be a member to view the live stream"
            redirect_to :root
        end
    end

end
