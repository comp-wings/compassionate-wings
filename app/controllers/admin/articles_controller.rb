class Admin::ArticlesController < Admin::AdminController

    def index
        @articles = Article.all
        render layout: 'layouts/admin'
    end

    def edit
        @article = Article.find(params[:id])
        render layout: 'layouts/admin'
    end

    def update
        Article.update params[:id], params.require(:article).permit(:title, :content, :subtitle, :premium)
        redirect_to :admin_articles
    end

    def new
        @article = Article.new
        render layout: 'layouts/admin'
    end

    def create
        article = Article.create! params.require(:article).permit(:title, :content, :subtitle, :premium)
        redirect_to :admin_articles
    end

    def toggle
        article = Article.find(params[:article_id]).toggle!(:hidden)

        redirect_to :admin_articles
    end

end
