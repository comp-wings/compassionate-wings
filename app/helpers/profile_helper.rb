module ProfileHelper
    def user_type(user)
        user.type.split('::')[1].capitalize
    end
end
