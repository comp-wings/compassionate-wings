class Article < ApplicationRecord
  has_rich_text :content

  @@preview_end_keyword = '^PREVIEW^'

  def preview
    source = content.body.fragment.source

    paywall_article = false
    preview_end = 0

    source.children.each_with_index do |child, index|
      if child.text.include? @@preview_end_keyword 
        paywall_article = true
        preview_end = index
        child.traverse do |n|
          if n.text?
            n.content = n.content.gsub @@preview_end_keyword, ''
          end
        end
      end
    end

    if paywall_article
      content.body.fragment.source.children = source.children[0..preview_end]
    end

    content
  end

end