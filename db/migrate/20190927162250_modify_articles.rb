class ModifyArticles < ActiveRecord::Migration[6.0]
  def change
    change_table :articles do |t|
      t.string :title
      t.boolean :members
    end
  end
end
