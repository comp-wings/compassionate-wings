class AddUserDefaultType < ActiveRecord::Migration[6.0]
  def up
    change_column_default(:users, :type, 2)
  end

  def down
    change_column_default(:users, :type, nil)
  end
end
