module Membership
    module Premium
        class << self

            MONTHLY_FEE = 3000

            # Customer info
            # https://github.com/square/square-ruby-sdk/blob/master/doc/customers.md#create-customer
            #
            # Card Info
            # https://github.com/square/square-ruby-sdk/blob/master/doc/customers.md#create-customer-card
            #
            # @returns [Hash] { customer_info: json, card_info: json }
            def upgrade_user(customer, card)
                customer_info = customers.create_customer(body: customer)
                customer_id = customer_info.data.customer[:id]
               
                card_info = customers.create_customer_card(customer_id: customer_id, body: card)

                Payments::Square.charge_user({
                    customer_id: customer_id,
                    cc: card_info.data.card[:id],
                    charge_id: SecureRandom.uuid,
                    amount: MONTHLY_FEE,
                    description: 'Premium membership purchase'
                })

                # Pass this return value into User.update(id, <value>)
                { 
                    type: 'User::Member',
                    customer_info: customer_info.data.customer.to_json, 
                    card_info: card_info.data.card.to_json
                }
            end

            private 

            def customers
                Payments::Square.client.customers
            end
        end
    end
end